package main

import (
	"flag"
	"fmt"
	"image"
	"os"

	"gocv.io/x/gocv"
)

func main() {
	tmpMat := gocv.NewMat()

	targetFile := flag.String("targetFile", "", "Video filename to be barcoded.")
	skipCount := flag.Int("skipCount", 500, "Number of frames to skip.")
	sliceWidth := flag.Int("sliceWidth", 4, "Width of each frame in pixels after it gets compressed into a layer in the final image.")
	flag.Parse()

	var fCount uint64 = 0
	var frameCount *uint64 = &fCount

	frameChannel := make(chan gocv.Mat)

	go procFrame(frameChannel, frameCount, *sliceWidth)

	vid, err := gocv.VideoCaptureFile(*targetFile)
	if err != nil {
		fmt.Println(err)
	}

	for vid.IsOpened() {
		//	go func() {
		vid.Read(&tmpMat)

		if *frameCount%uint64(*skipCount) == 0 {
			frameChannel <- tmpMat
			fmt.Print("\r", *frameCount)
		}
		*frameCount++
		//	}()
	}

	_ = <-frameChannel
}

func procFrame(inChan chan gocv.Mat, frameCount *uint64, sliceWidth *int) {

	initFrame := <-inChan
	outFrame := gocv.NewMatWithSize(initFrame.Rows(), 4, initFrame.Type())
	resizePoint := image.Point{Y: initFrame.Rows(), X: sliceWidth}
	count := 0

	for iter := range inChan {
		if iter.Empty() {
			fmt.Println("EMPTY FRAME. QUITTING.")
			break
		}
		count++
		gocv.Resize(iter, &iter, resizePoint, 0, 0, 0)
		if iter.Cols() < 1+sliceWidth {
			gocv.Hconcat(outFrame, iter, &outFrame)
		}
	}
	gocv.IMWrite("Output.jpg", outFrame)
	inChan <- initFrame
	os.Exit(0)
}
