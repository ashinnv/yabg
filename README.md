# Disclaimer:
## This is not an original idea of mine. The oldest reference that I could easily find was at https://moviebarcode.tumblr.com/

# Instructions:

To run this program, you need to have the golang programming language installed, Opencv 4 or newer and the gocv go package as described at gocv.io

Once you have the dependencies installed, open a terminal in the directory where the .go file and your target video are and type "go run -fileName" followed by the path to the video. 

Other options are "skipCount" which is an integer number that determines how many frames to skip (suggested is 100) and an option to set layer width in the output.
